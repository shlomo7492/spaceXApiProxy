const express = require("express");
const app = express();
const axios = require("axios");
const cors = require("cors");
require("dotenv").config(".env");

app.use(cors({
    origin:"*" //Sets CORS for  request from all origins
}))

app.get(":endpoint([\\/\\w\\.-]*)", (req,res)=>{
    let queryString = "?";
    //Composing our query string
    for(let key in req.query){
        if(queryString.length>1){
            queryString+="&"+key+"="+req.query[key];
        } else{
            queryString+=key+"="+req.query[key];
        }
    }
    //Here we compose our endpoint url
    const endPointUrl = process.env.BASE_URL+req.params.endpoint+queryString;

    //Here we do actual spaceX api request using endPointUrl const 
    axios.get(endPointUrl)
        .then(response=>res.json(response.data))
        .catch(error=>res.json(error));
})

app.listen(3001);